package com.joe.dbtester;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class DAO {
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	public List<Map<String,Object>> queryIt(String clubId, String useCaseId, String useCaseSubCategory) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("clubId", clubId)
			  .addValue("useCaseId", useCaseId)
			  .addValue("useCaseSubCategory", useCaseSubCategory);
		
		return jdbcTemplate.queryForList(
				"SELECT conn.ClubId,conn.UsecaseId,conn.Priority,conn.EndpointURL,conn.EndpointTimeout,conn.ResponseType,conn.AuthenticationType, " + 
				"conn.UseCaseSubcategory,conn.ClientID,conn.ClientSecret,conn.JwtSignKey,conn.JwtAlgorithm,endTypes.isInternal,endTypes.Protocol,endTypes.EndPointType " + 
				"FROM ClubConnectivity conn " + 
				"inner join EndPointTypes endTypes on " + 
				"conn.EndpointTypeId = endTypes.EndpointTypeId " + 
				"where ClubId=:clubId and UsecaseId=:useCaseId and UseCaseSubcategory=:useCaseSubCategory " + 
				"order by conn.Priority", params);
	}
}

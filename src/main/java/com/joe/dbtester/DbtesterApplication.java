package com.joe.dbtester;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbtesterApplication {
	@Autowired
	DAO dao;
	
	public static void main(String[] args) {
		SpringApplication.run(DbtesterApplication.class, args);
		
	}
	
	
	
	
	/* - for CommandLineRunner setup 
	@Override
	public void run(String... args) throws Exception {
		
		
		List<Map<String,Object>> result = dao.queryIt("004", "MEMBERSHIP_SEARCH", "IDSEARCH");
		System.out.println(result);
	}
	*/
	

}

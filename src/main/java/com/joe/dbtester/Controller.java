package com.joe.dbtester;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	@Autowired
	DAO dao;
	
	@RequestMapping(path="/{club}/{case}/{subcase}")
	public List<Map<String,Object>> getData(@PathVariable("club") String clubId, 
											@PathVariable("case") String useCaseId,
											@PathVariable("subcase") String useCaseSubCategory) {
		return dao.queryIt(clubId, useCaseId, useCaseSubCategory);
	}
}
